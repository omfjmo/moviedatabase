﻿using System;
using System.Collections.Generic;

namespace FilmesAdatbazis.MovieModels
{
    public partial class Actor
    {
        public Actor()
        {
            Performances = new HashSet<Performance>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;

        public virtual ICollection<Performance> Performances { get; set; }

        public override string ToString()
        {
            return $"{Id} {Name}";
        }
    }
}
