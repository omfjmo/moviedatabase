﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace FilmesAdatbazis.MovieModels
{
    [Serializable]
    [XmlRoot("Movies")]
    public partial class Movie
    {
        public Movie()
        {
            Performances = new HashSet<Performance>();
        }

        [XmlIgnore]
        [IgnoreDataMember]
        public int Id { get; set; }

        [XmlElement("Title")]
        public string Title { get; set; } = null!;

        [XmlIgnore]
        [IgnoreDataMember]
        public int GenreId { get; set; }

        [XmlIgnore]
        [IgnoreDataMember]
        public string? Director { get; set; }

        [XmlIgnore]
        [IgnoreDataMember]
        public int? Release { get; set; }

        [XmlElement("CreationDate")]
        public DateTime CreationDate { get; set; }

        [XmlIgnore]
        [IgnoreDataMember]
        public virtual Genre Genre { get; set; } = null!;

        [XmlIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Performance> Performances { get; set; }

        public override string ToString()
        {
            return $"{Title} {Genre.Name} {Director} {Release}";
        }
    }
}
