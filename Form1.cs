using FilmesAdatbazis.MovieModels;
using FilmesAdatbazis.Movies;

namespace FilmesAdatbazis
{
    public partial class Form1 : Form
    {
        Helper h;
        Movie m;

        public Form1()
        {
            InitializeComponent();
            h = new();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 f2 = new();
            f2.ShowDialog();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            h.LoadData(listViewMovie);

            MyMovies movies = new();

            foreach (var genre in movies.Genres)
            {
                cbGenre.Items.Add(genre.Name);
            }

            foreach (var actor in movies.Actors)
            {
                cbActor.Items.Add(actor.Name);
            }
        }

        private void cbGenre_SelectedValueChanged(object sender, EventArgs e)
        {
            h.FilterGenre(cbGenre, listViewMovie);
        }

        private void button2_Click(object sender, EventArgs e)
        {
        
        }

        private void button3_Click(object sender, EventArgs e)
        {
            h.DeleteMovie(listViewMovie);
        }
        private void cbActor_SelectedValueChanged(object sender, EventArgs e)
        {
            h.FilterActors(cbActor, listViewMovie);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            listViewMovie.Items.Clear();
            h.LoadData(listViewMovie);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            h.SaveMovies();
        }
    }
}