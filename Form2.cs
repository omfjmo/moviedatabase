﻿using FilmesAdatbazis.Movies;
using FilmesAdatbazis.MovieModels;

namespace FilmesAdatbazis
{
    public partial class Form2 : Form
    {
        MyMovies movies;
        Helper h;
      
        public Form2()
        {
            InitializeComponent();
            movies = new();
            h = new();   
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            foreach (var genre in movies.Genres)
            {
                cbGenre.Items.Add(genre);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Movie movie = new();
            
            if(tbTitle.Text == null || cbGenre.SelectedItem == null)
            {
                MessageBox.Show("Töltse ki a kötelező mezőket!");
                return;
            }

            movie.Title = tbTitle.Text.Trim();
            movie.GenreId = ((Genre)cbGenre.SelectedItem).Id;
            movie.Director = tbDirector.Text;
            movie.Release = (int)nudRelease.Value;
            movie.CreationDate = DateTime.Now;
            
            h.CreateMovie(movie);
        }   
    }
}
