﻿using FilmesAdatbazis.MovieModels;
using FilmesAdatbazis.Movies;
using Microsoft.EntityFrameworkCore;
using System.Xml.Serialization;

namespace FilmesAdatbazis
{
    public class Helper
    {
        MyMovies movies;

        public Helper()
        {
            movies = new();
        }
        public void LoadData(ListView listViewMovie)
        {
            var query = from movie in movies.Movies
                        join genre in movies.Genres
                        on movie.GenreId equals genre.Id
                        orderby movie.Title
                        select new
                        {
                            id = movie.Id,
                            title = movie.Title,
                            genreName = genre.Name,
                            director = movie.Director,
                            release = movie.Release
                        };

            foreach (var movie in query)
            {
                string[] row = {

                    movie.title.Trim(),
                    movie.genreName,
                    movie.director,
                    movie.release.ToString()                
                };

                var item = new ListViewItem(row);
                listViewMovie.Items.Add(item);
            }
        }

        public void CreateMovie(Movie movie)
        {
             try
             {
                 movies.Movies.Add(movie);
                 movies.SaveChanges();
                 MessageBox.Show("Sikeres hozzáadás!");
             }
             catch (DbUpdateException)
             {
                 MessageBox.Show("Sikertelen hozzáadás!");
             }
        }
    

        public void DeleteMovie(ListView listViewMovie)
        {
            if (listViewMovie.SelectedItems.Count != 0)
            {
                if (MessageBox.Show("Biztos törli?", "Törlés", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    var report = (from movie in movies.Movies
                                  where movie.Title.Trim() == listViewMovie.SelectedItems[0].Text
                                  select movie).Single();

                    try
                    {
                        movies.Movies.Remove(report);
                        movies.SaveChanges();
                        MessageBox.Show("Sikeres törlés!");
                    }
                    catch (DbUpdateException)
                    {
                        MessageBox.Show("Sikertelen törlés!");
                    }
                }
            }
        }

        public void FilterGenre(ComboBox cbGenre, ListView listViewMovie)
        {
            var query = from movie in movies.Movies
                        join genre in movies.Genres
                        on movie.GenreId equals genre.Id
                        where cbGenre.SelectedItem.ToString() == genre.Name
                        select new
                        {
                            title = movie.Title.Trim()
                        };

            listViewMovie.Items.Clear();
            foreach (var item in query)
            {
                listViewMovie.Items.Add(item.title);
            }
        }

        public void FilterActors(ComboBox cbActor, ListView listViewMovie)
        {
            var query = from p in movies.Performances
                        join movie in movies.Movies
                        on p.MovieId equals movie.Id
                        join actor in movies.Actors
                        on p.ActorId equals actor.Id
                        where cbActor.SelectedItem.ToString() == actor.Name
                        select new
                        {
                            title = movie.Title.Trim()
                        };

            listViewMovie.Items.Clear();
            foreach (var item in query)
            {
                listViewMovie.Items.Add(item.title);
            }
        }

        public void SaveMovies()
        {
            MyMovies movies = new();
            List<Movie> list = new();

            using (TextWriter sw = new StreamWriter("movies.xml"))
            {
                try
                {
                    foreach (Movie item in movies.Movies)
                    {
                        list.Add(item);
                    }
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Movie>));
                    serializer.Serialize(sw, list);

                    MessageBox.Show("Sikeres exportálás!");
                }
                catch (Exception)
                {
                    MessageBox.Show("Sikertelen exportálás!");
                }
            }
        }
    }
}
